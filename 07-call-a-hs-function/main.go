package main

import (
	"fmt"
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {

	// get items of array
	firstName := args[0].Index(0)
	lastName := args[0].Index(1)

	return []interface{} {
		"Hello ",
		firstName,
		lastName,
		"by Jul13nc",
	}

}

func main() {
	fmt.Println(js.Global().Call("sayHello","From GO"))

	message := js.Global().Get("message").String()
	fmt.Println("message (before): ", message)

	js.Global().Set("message","Updated from go")

	bill := js.Global().Get("bill")
	fmt.Println("bill (before): ",bill)

	bill.Set("firstName","Bill")
	bill.Set("lastName","Ballantine")
	

	<-make(chan bool)
}
