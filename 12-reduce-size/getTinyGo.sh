tinygo_version="0.21.0"
# Download wasm_exec.js
wget https://raw.githubusercontent.com/tinygo-org/tinygo/v${tinygo_version}/targets/wasm_exec.js --output-document=./${directory_name}/wasm_exec.js
