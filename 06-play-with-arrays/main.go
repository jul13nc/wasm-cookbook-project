package main

import (
	"syscall/js"
)

func Hello(this js.Value, args []js.Value) interface{} {

	// get items of array
	firstName := args[0].Index(0)
	lastName := args[0].Index(1)

	return []interface{} {
		"Hello ",
		firstName,
		lastName,
		"by Jul13nc",
	}

}

func main() {
	js.Global().Set("Hello", js.FuncOf(Hello))

	<-make(chan bool)
}
