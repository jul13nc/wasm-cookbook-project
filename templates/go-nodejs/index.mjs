import fs from "fs"
import * as _ from "./wasm_exec.js"

function runWasm(wasmFile, args) {
  const go = new Go()

  return new Promise((resolve, reject) => {
    WebAssembly.instantiate(wasmFile, go.importObject)
    .then(result => {
      if(args) go.argv = args
      go.run(result.instance) 
      resolve(result.instance)
    })
    .catch(error => {
      reject(error)
    })
  })
}


/*
try {
  const wasmFile = fs.readFileSync('./main.wasm')
  runWasm(wasmFile).then(wasm => {

  }).catch(error => {
    console.log("ouch", error)
  }) 
} catch (err) {
  console.error(err)
}
*/

fs.readFile('./main.wasm', (err, wasmFile) => {
  if (err) {
    console.error(err)
    return
  }
  runWasm(wasmFile).then(wasm => {
    // foo
  }).catch(error => {
    console.log("ouch", error)
  }) 
})
