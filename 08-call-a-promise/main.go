package main

import (
	"fmt"
	"syscall/js"
)

func main() {
	thenFun := func(this js.Value, args []js.Value) interface{} {
		fmt.Println("All good:", args[0].String())
		return ""
	}

	catchFun := func(this js.Value, args []js.Value) interface{} {
		fmt.Println("Ouch:", args[0].Get("message"))
		return ""
	}

	js.Global().Call("compute", false).Call("then", js.FuncOf(thenFun)).Call("catch", js.FuncOf(catchFun))

	js.Global().Call("compute", true).Call("then", js.FuncOf(thenFun)).Call("catch", js.FuncOf(catchFun))

	<-make(chan bool)
}
