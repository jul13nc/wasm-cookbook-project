#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo '😡 you must give a project name'
    exit 0
fi

directory_name=$1
go_version="1.17"
tinygo_version="0.21.0"

cp templates/tinygo-browser/ ${directory_name} -r

# Create go.mod
cat > ${directory_name}/go.mod <<- EOM
module ${directory_name}

go ${go_version}
EOM

# Download wasm_exec.js
wget https://raw.githubusercontent.com/tinygo-org/tinygo/v${tinygo_version}/targets/wasm_exec.js --output-document=./${directory_name}/wasm_exec.js


